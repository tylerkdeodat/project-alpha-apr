* [X] - create a list view for the project model

* [X] - register view with a path in urls.py
path("", name="list_projects")

* [X] - register projects app path in tracker project urls.py
path("projects/", )

* [X] - create a template for the list view

* [X] - template should have fundamental 5
main tag that contains 
    a div tag that contains
        an h1 tag w/ "My Projects"
        if there are no projects created then 
            a p tag w/ "You are not assigned any projects"
        otherwise
            a table that has two columns
                header "Name" and rows with the name of the projects
                header "Number of Tasks" 

* [X] - register the LoginView in accounts/urls.py w/ 
path("login/", ...... name="login")

* [X] - inlcude the URL patterns in the tracker project for accounts app
path("accounts/")

* [X] - create a templates directory under accounts

* [X] - create a registration directory under templates in accounts app

* [X] - create an html template named login.html in the registration directory

* [X] - put a post form in the login.html as well as fundamental 5

* [X] - in tracker settings.py create and set variable LOGIN_REDIRECT_URL = "home"

* [X] - in accounts/urls.py import LogoutView 

* [X] - register that view in urlpatterns w/ path("logout/" ... name="logout")

* [X] - in project seeting.py file set LOGOUT_REDIRECT_URL = "login"

* [X] - in accounts/views.py create a function view

* [X] - import UserCreationForm from django.contrib.auth.forms 

* [X] - UserCreationForm has three fields: username; password1; and password2.
PSEUDOCODE:
def signup(request):
    if request.method == "POST":
        form = UserCreationFor(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm(request.POST)
    context = {"form": form}
    return render(request, "registration/signup.html", context)

* [X] - create an html page called signup.html in registration directory

* [X] - put a post form as well as fundamental 5

* [X] - create a task model in tasks.models

* [X] - Create a project detail view

* [X] - must have LoginRequiredMixin

* [X] - in projects.urls.py register detail view with 
path("<int:pk>/", ... name="show_project")

* [X] - create a template called detail.html

* [X] - update list.html to show the number of tasks for a project

* [X] - update list.html with a link from the project name to the detail view for that project

* [X] - Create a ProjectCreateView with field = [name, description, members]

* [X] - must have LoginRequiredMixin 

* [X] - redirect to detail page for that project success_url = reverse_lazy("show_project")

* [X] - register path in project/urls.py w/ path("create/", ...., name="create_project")

* [X] - create html template called create.html

* [X] - add a link in the list.html that goes to the new create.html

* [X] - create a TaskCreateView in tasks/views.py for all properties except is_completed: field = []

* [X] - needs to have LoginRequiredMixin 

* [X] - must redirect to the detail page for the tasks project

* [X] - register path in tasks/urls.py w/ path("create/", ..., name="create_task")

* [X] - include tasks app urls in tracker project urls.py

* [X] - create a template for the create view 

* [X] - add a link to create a task in the project detail page

* [X] - create a TaskListView in tasks/views.py w/ assignee = currently logged in user(assignee = self.request.user)

* [X] - must have LoginRequiredMixin 

* [X] - register view in  tasks/urls.py w/ path("mine/",..., name="show_my_tasks")

* [X] - create an html template 

* [X] - Create a TaskUpdateView that is ONLY using the is_completed field

* [X] - redirects to "show_my_tasks" success_url property 

* [X] - do not need to make a template

* [X] - modify 

* [X] - Install markdownify 

* [X] - put it in installed apps

* [X] - in tracker/settings.py add :
MARKDOWNIFY = {
    "default": {
        "BLEACH": False
    }
}